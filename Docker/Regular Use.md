

# Regular Use

## 一、基本

### 1、常用命令

| 命令                                          | 功能               | 备注                                              |
| --------------------------------------------- | ------------------ | ------------------------------------------------- |
| docker search image_name                      | 查询相关镜像       | 默认从docker_hub获取                              |
| docker pull image_name                        | 下载镜像           |                                                   |
| docker images                                 | 列出已下载镜像     |                                                   |
| docker rmi image_id                           | 删除镜像           | -f : 强制                                         |
| docker ps                                     | 查询正在运行容器   | -a : 所有                                         |
| docker run image_name                         | 运行(创建)容器     | 参数见下↓↓↓                                       |
| docker start\|stop container_id\|name         | (停止\|启动)容器   |                                                   |
| docker rm container_id\|name                  | 删除容器           | -f : 强制                                         |
| docker logs container_id\|name                | 查看容器日志       | -f : 监听方式 --tail 10: 后10行                   |
| docker stats container_id\|name               | 容器状态           | 内存，磁盘使用情况等                              |
| docker exec -it container_id\|name /bin/bash  | 进入容器           | -i : 交互模式<br />-t : 进入命令行                |
| docker save image_name\|id >> export_name.tar | 将镜像导出至文件   |                                                   |
| docker load < image_file                      | 将镜像文件导入     |                                                   |
| docker  commit container_id\|name             | 将容器封装成镜像   |                                                   |
| docker rm -f `docker ps -a -q`                | 删除所有容器       |                                                   |
| docker rm `docker ps -a -q`                   | 删除非运行的容器   |                                                   |
| docker container update                       | 更新已启动容器配置 | docker container update --restart=always 容器名字 |

###  2、投产常用

| 参数                         | 作用         | 备注               |
| ---------------------------- | ------------ | ------------------ |
| --restart=always             | 自动启动     |                    |
| -v local_path:container_path | 挂在目录     |                    |
| -p local_port:container_port | 绑定端口     |                    |
| --net=network_type           | 设置网络模式 | host，none，bridge |

>  谨慎修改iptables、network等服务，可能会造成docker网桥异常。

## 二、经常使用



#### 跟换镜像源（加速docker pull）

```shell
# 阿里镜像加速地址 or 私有仓库地址
AliRegistry=https://1f8lg9kf.mirror.aliyuncs.com
mkdir -p /etc/docker
echo -e  "{\n\"registry-mirrors\": [\"$AliRegistry\"]\n} " > /etc/docker/daemon.json
systemctl daemon-reload
systemctl restart docker
```



#### 批量操作容器

```shell
# 停止
docker stop $(docker ps -a -q)

# 删除
docker rm $(docker ps -a -q)
```



#### 批量清理容器日志

```shell
# clean_docker_log.sh
# 获取日志大小
logs=$(find /var/lib/docker/containers/ -name *-json.log)  
for log in $logs  
do  
	ls -lh $log   
done  

# 清理日志
for log in $logs  
do  
  echo "clean logs : $log"  
  cat /dev/null > $log  
done  
```



#### 清理未使用镜像

```shell
docker image prune -a 
# -f 强制
```



#### 修改容器默认存储路径

```shell
vim /etc/docker/daemon.json
# 加入以下内容
{
    "graph":"/data/docker"
}
```

