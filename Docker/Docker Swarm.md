# Docker Swarm

##### 主机

- 192.168.239.10 master
- 192.168.239.20 node1
- 192.168.239.30 node2

##### 基本配置（各节点）

```shell
# 安装及配置docker服务
# 配置docker-repo源
[dockerrepo]
name=Docker Repository
baseurl=https://yum.dockerproject.org/repo/main/centos/7/
enabled=1
gpgcheck=1
gpgkey=https://yum.dockerproject.org/gpg
# 安装docker-ce
yum -y install docker-ce
```

### 创建 swarm 集群

##### master 节点

```shell
# 创建swarm集群
docker swarm init --advertise-addr 192.168.239.10
```

##### node 各节点

```shell
# tocken 从master节点copy
# 加入swarm集群
docker swarm join --token SWMTKN-1-5ptr0ongkmv96pbqzjaf8tu745f7k3ehcm2ytvti3iaz2shdha-1etx7xij9zskvanlz41jxzzy3 192.168.239.10:2377
```

| 命令                       | 解释                |
| -------------------------- | ------------------- |
| docker swarm leave --force | 强制退出 swarm 集群 |
| docker node ls             | 查看 swarm 节点     |
| docker service create      | 创建集群应用        |
| docker service ls          | 查看集群应用        |
| docker stack deploy        | 扩展集群应用        |
| docker swarm init          | 初始化 swarm 集群   |
| docker swarm join          | 加入 swarm 集群     |

### 创建集群应用

##### master 节点

```shell
# 创建nginx应用，配置两个节点，"--name"指定应用名称
docker service create --replicas 2  -p 81:80 --name nginx2 nginx:1.13.7-alpine
```
