# Docker Machine 



## 一、安装

> docker-machine工具下载地址：https://github.com/docker/machine/releases/
>
> 驱动下载地址：https://github.com/machine-drivers/

macos

```shell
wget https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-Darwin-x86_64
mv docker-machine-Darwin-x86_64 /usr/local/bin/docker-machine
chmod +x !$

# vmware lib
wget https://github.com/machine-drivers/docker-machine-driver-vmware/releases/download/v0.1.0/docker-machine-driver-vmware_darwin_amd64
mv docker-machine-driver-vmware_darwin_amd64 /usr/local/bin/docker-machine-driver-vmware
chmod +x !$
```



## 二、使用

### 创建Docker虚拟机

```shell
docker-machine create --driver vmware test
# 默认密码 tcuser

# 进入容器内
docker-machine  ssh test 

# 修改镜像加速地址
echo "EXTRA_ARGS=\"--registry-mirror=http://e69374c5.m.daocloud.io\"" >> /var/lib/boot2docker/profile


```



### 常用参数

#### create

| 参数                 | 说明         | 示例 |
| -------------------- | ------------ | ---- |
| --driver , -d "none" | 设置驱动     |      |
| --engine-env         | 设置环境变量 |      |
|                      |              |      |
|                      |              |      |
|                      |              |      |
|                      |              |      |
|                      |              |      |
|                      |              |      |
|                      |              |      |













