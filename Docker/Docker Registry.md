# Docker Registry

一、安装Docker服务

```shell
# 安装依赖
yum install -y yum-utils device-mapper-persistent-data lvm2
# 导入docker—yum源
yum-config-manager --add-repo  https://download.docker.com/linux/centos/docker-ce.repo
# 安装docker-ce
yum -y install docker-ce
```

二、安装Docker Registry

```shell
# 下载 registry镜像
docker pull registry
# 启动容器
docker run -d -p 5000:5000 -v /myregistry:/var/lib/registry --restart=always registry
# 检测服务是否运行
curl http://{ip}:5000/v2/_catalog
```

三、上传

```shell
# 给镜像打标签
docker tag tomcat 127.0.0.1:5000/tomcat:v1
# 上传镜像
docker push 127.0.0.1:5000/tomcat:v1
```

四、下载

```shell
# 下载镜像
docker pull 127.0.0.1:5000/tomcat:v1
```

