# Docker Api

## 一、开启Docker-api

```bash
#在`ExecStart` 参数后面添加 
vi /usr/lib/systemd/system/docker.service
-H tcp://0.0.0.0:2375 -H unix:///var/run/docker.sock
# 或者
vi /etc/docker/daemon.json
{
	"hosts": ["tcp://0.0.0.0:2375", "unix:///var/run/docker.sock"]
}

systemctl daemon-reload
systemctl restart docker
```

