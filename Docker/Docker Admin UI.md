# Docker Admin UI

## 一、Portainer(汉化版)

### 1、单机版

```shell
# 下载镜像
docker pull portainer/portainer:alpine

DOCKER_NAME=portainer

# 挂载目录
mkdir -p /data/$DOCKER_NAME/public
mkdir -p  /data/$DOCKER_NAME/data

# 汉化文件
wget -O- https://raw.githubusercontent.com/renyinping/portainer-cn/master/Portainer-CN.zip

mv Portainer-CN.zip /data/$DOCKER_NAME/public/
cd !$
unzip Portainer-CN.zip


# 运行
docker run -d \
-p 9000:9000 \
--name portainer \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /data/$DOCKER_NAME/data:/data \
-v /data/$DOCKER_NAME/public:/public \
-v /etc/localtime:/etc/localtime \
portainer/portainer:alpine

# Tmp
docker run -d \
-p 3001:9000 \
--name portainer \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v /data/$DOCKER_NAME/data:/data \
-v /data/$DOCKER_NAME/public:/public \
-v /etc/localtime:/etc/localtime \
portainer/portainer:alpine
```



## 二、DockerUI

## 三、Shipyard

