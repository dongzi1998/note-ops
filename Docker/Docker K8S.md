# K8S

| master | 192.168.239.10 |
| ------ | -------------- |
| node1  | 192.168.239.20 |
| node2  | 192.168.239.30 |



## 一、搭建（Centos7）

### 1、安装docker-ce

`master+node`

```shell
#!/bin/bash
#安装配置docker-ce

# 安装依赖工具
yum install -y yum-utils device-mapper-persistent-data lvm2
# 安装docker-repo源
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo

# 安装docker-ce
yum install docker-ce-18.09.6 docker-ce-cli-18.09.6 containerd.io
	
# 启动及配置自启动
systemctl start docker
systemctl enble docker

```

### 2、k8s集群

##### a、安装配置etcd服务

`master`

> 通过key-value的方式管理配置

```shell
ETCD_DATA_DIR="/var/lib/etcd/default.etcd"
# 配置监听端口
ETCD_LISTEN_CLIENT_URLSetcdctl get test/test0 ="http://0.0.0.0:2379,http://0.0.0.0:4001"
# 修改etcd服务名称
ETCD_NAME="master"
ETCD_ADVERTISE_CLIENT_URLS="http://master:2379,http://master:4001"

# 启动
systemctl start etcd && systemctl enable etcd 

# 测试
etcdctl set test/test0 0
etcdctl get test/test0 
```

##### b、安装kubernetes

`master+node`

```shell
# 配置阿里k8s-repo源
cat >> /etc/yum.repos.d/kubernetes.repo << EOF
[kubernetes]
name=Kubernetes
baseurl=https://mirrors.aliyun.com/kubernetes/yum/repos/kubernetes-el7-x86_64/
enabled=1
gpgcheck=0
EOF
# 安装k8s
yum install kubernetes
```

##### c、修改配置

`master`

```shell

mv /etc/kubernetes/apiserver /etc/kubernetes/apiserver_bak
cat <<EOF > /etc/kubernetes/apiserver1
KUBE_API_ADDRESS="--insecure-bind-address=0.0.0.0"      ## kube启动时绑定的地址
KUBE_API_PORT="--port=8080"
KUBELET_PORT="--kubelet-port=10250"
KUBE_ETCD_SERVERS="--etcd-servers=http://master:2379"　　## kube调用etcd的url
KUBE_SERVICE_ADDRESSES="--service-cluster-ip-range=10.254.0.0/16" # 后面flanneld配置相同
KUBE_ADMISSION_CONTROL="--admission-control=NamespaceLifecycle,NamespaceExists,LimitRanger,SecurityContextDeny,ResourceQuota"
EOF

mv /etc/kubernetes/config /etc/kubernetes/config_bak
cat <<EOF > /etc/kubernetes/config
KUBE_LOGTOSTDERR="--logtostderr=true"
KUBE_LOG_LEVEL="--v=0"
KUBE_ALLOW_PRIV="--allow-privileged=false"
KUBE_MASTER="--master=http://master:8080"
EOF

```

`node`

```shell
mv /etc/kubernetes/kubelet  vim /etc/kubernetes/kubelet_bak
cat <<EOF > /etc/kubernetes/kubelet
KUBELET_ADDRESS="--address=0.0.0.0"
KUBELET_HOSTNAME="--hostname-override=node1"
KUBELET_API_SERVER="--api-servers=http://master:8080"
KUBELET_POD_INFRA_CONTAINER="--pod-infra-container-image=registry.access.redhat.com/rhel7/pod-infrastructure:latest"
KUBELET_ARGS=""
EOF

mv /etc/kubernetes/config  /etc/kubernetes/config_bak
cat <<EOF > /etc/kubernetes/config
KUBE_LOGTOSTDERR="--logtostderr=true"
KUBE_LOG_LEVEL="--v=0"
KUBE_ALLOW_PRIV="--allow-privileged=false"
KUBE_MASTER="--master=http://master:8080"
EOF
```

##### d、Etcd服务中添加flannel网络

`master`

```shell
etcdctl mk /atomic.io/network/config '{ "Network": "10.254.0.0/16" }'
```

##### e、安装配置flanneld服务

`master+node`

> flanneld服务用于连接各节点的docker0网桥，保持pod节点间的通信

```shell
yum install flannel
mv /etc/sysconfig/flanneld  /etc/sysconfig/flanneld_bak
cat <<EOF > /etc/sysconfig/flanneld
FLANNEL_ETCD_ENDPOINTS="http://master:2379"
FLANNEL_ETCD_PREFIX="/atomic.io/network"
EOF
systemctl enable flanneld.service 
systemctl restart flanneld.service 
systemctl restart docker
```

##### f、启动k8s集群

`master`

```shell
systemctl enable kube-apiserver.service 
systemctl enable kube-controller-manager.service 
systemctl enable kube-scheduler.service
systemctl start kube-apiserver.service 
systemctl start kube-controller-manager.service 
systemctl start kube-scheduler.service
```

`node`

```shell
systemctl enable kubelet.service
systemctl enable kube-proxy.service
systemctl start kubelet.service
systemctl start kube-proxy.service
```



