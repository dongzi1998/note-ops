# Logstash



## 一、安装

### Docker

```shell
DOCKER_NAME=xfxp-logstash-prod
docker pull logstash:7.6.0
mkdir -p /data/$DOCKER_NAME/
mkdir -p /data/$DOCKER_NAME/conf.d

echo "path.config: /usr/share/logstash/conf.d/*.conf" >> /data/$DOCKER_NAME/logstash.yml
echo "path.logs: /var/log/logstash" >> /data/$DOCKER_NAME/logstash.yml

# 编辑配置文件
vi /data/$DOCKER_NAME/conf.d/demo-logstash.conf

docker run --name $DOCKER_NAME \
-it \
--restart=always \
-p 5044:5044 \
-p 5045:5045 \
-p 5959:5959 \
-v /data/$DOCKER_NAME/logstash.yml:/usr/share/logstash/config/logstash.yml \
-v /data/$DOCKER_NAME/conf.d/:/usr/share/logstash/conf.d/ \
-v /etc/localtime:/etc/localtime \
-d logstash:7.6.0
```



## 二、使用

### demo-logstash.conf

```shell
input {
  tcp {
    port => 5959
    mode => "server"
    tags => ["tags"]
    codec => json_lines
  }
}
output {
  elasticsearch {
    hosts => ["127.0.0.1:9200"]
    index => "demo" #对日志进行索引归档
  }
  stdout {
    codec => rubydebug
  }
}
```





## 三、常见问题

