# Kibana



## 一、安装

### Docker

```shell
docker pull kibana:7.6.0

DOCKER_NAME=kibana
ES_IP=

docker run --name $DOCKER_NAME \
--restart=always \
-p 5601:5601 \
-e ELASTICSEARCH_HOSTS=http://$ES_IP:9200  \
-v /etc/localtime:/etc/localtime \
-d kibana:7.6.0
```



## 二、配置详解

```yaml

```







## 三、问题汇总

### Data too large

```shell
# 清理filed data cache缓存
curl -XPOST 'http://localhost:9200/_cache/clear?fielddata=true'

# 设置filed data cache不要占用过大jvm内存
curl -XPUT "localhost:9201/_cluster/settings" -H 'Content-Type: application/json' -d '{
"persistent" : {
"indices.breaker.fielddata.limit" : "70%"
}
}'

# 调整es内存
# docker启动es时添加参数
# 生产建议4g以上内存配置
-e ES_JAVA_OPTS="-Xms4g -Xmx4g" \
```

