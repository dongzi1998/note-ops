# Elasticsearch



## 一、安装

### Docker

```shell
DOCKER_NAME=jzar-elasticsearch-prod
mkdir -p /data/$DOCKER_NAME/config
mkdir -p /data/$DOCKER_NAME/data
mkdir -p /data/$DOCKER_NAME/plugins

echo "http.host: 0.0.0.0" >> /data/$DOCKER_NAME/config/elasticsearch.yml
chmod 777 !$

docker pull elasticsearch:7.6.0
docker run --name $DOCKER_NAME \
-p 9200:9200  \
-p 9300:9300  \
-e "discovery.type=single-node" \
-e ES_JAVA_OPTS="-Xms64m -Xmx128m" \
-v /data/$DOCKER_NAME/config/elasticsearch.yml:/usr/share/elasticsearch/config/elasticsearch.yml \
-v /data/$DOCKER_NAME/data:/usr/share/elasticsearch/data \
-v /mydata/$DOCKER_NAME/plugins:/usr/share/elasticsearch/plugins -d elasticsearch:7.6.0

```





## 二、使用



### 配置

```yml
# 设置绑定地址
http.host: 0.0.0.0
# 避免发生OOM
indices.breaker.total.limit: 70%
# 最久未使用的 fielddata 会被回收
indices.fielddata.cache.size: 25%
# fielddata 断路器
indices.breaker.fielddata.limit: 40%
# request 断路器
indices.breaker.request.limit: 40%
```

`内存配置`

```shell
# docker容器内
/usr/share/elasticsearch/config/jvm.options
-Xms8g
-Xmx8g
# 重启容器

```



## 常见问题

