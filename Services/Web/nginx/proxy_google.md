# Nginx反向代理到不可访问网站（0）



```shell
# 下载nginx-1.7.8版本及
# 拉取 ngx_http_google_filter_module和ngx_http_substitutions_filter_module 插件
wget http://nginx.org/download/nginx-1.7.8.tar.gz
git clone https://github.com/cuber/ngx_http_google_filter_module
git clone https://github.com/yaoweibin/ngx_http_substitutions_filter_module

# 编译安装
tar nginx-1.7.8.tar.gz
cd nginx-1.7.8
./configure \
--user=nginx                          \
--group=nginx                         \
--prefix=/usr/local/nginx             \
--sbin-path=/usr/sbin/nginx           \
--conf-path=/etc/nginx/nginx.conf     \
--pid-path=/var/run/nginx.pid         \
--lock-path=/var/run/nginx.lock       \
--error-log-path=/var/log/nginx/error.log \
--http-log-path=/var/log/nginx/access.log \
--with-http_gzip_static_module        \
--with-http_stub_status_module        \
--with-http_ssl_module                \
--with-pcre                           \
--with-file-aio                       \
--with-http_realip_module             \
--without-http_scgi_module            \
--without-http_uwsgi_module           \
--without-http_fastcgi_module         \
--add-module=/root/ngx_http_google_filter_module \
--add-module=/root/ngx_http_substitutions_filter_module 
make && make install 

```

```nginx
  server {
    listen       80;
    server_name  g.dongzi.online;#此处换成该vps的外网ip（前一步得到的那个），或者自己的网址
    resolver 8.8.8.8;

    location / {
      # 加一些限制
      google on;
      google_scholar on;
      # set language to Chinese (Simplified)
      google_language zh-CN;
    	if ($http_referer != 'www.daived.com' ){
				proxy_pass http://www.baidu.com;
    	}
    }
  }

```

