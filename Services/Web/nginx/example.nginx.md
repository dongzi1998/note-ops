# nginx 配置模板

#### nginx.conf

```nginx

user  nginx;
worker_processes  1;

events {
    worker_connections  1024;
}

http {
    include       mime.types;
    include       conf.d/*.conf;
    default_type  application/octet-stream;
    client_max_body_size  1000m;
    sendfile        on;
    proxy_read_timeout 600;
    proxy_send_timeout 600;
    keepalive_timeout  600;
}

```

#### conf.d/example.conf

```nginx

server {
  listen 80;
  server_name localhost;
  root   html;
  location / {
  index  index.html index.htm;
}

```



