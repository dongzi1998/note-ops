# Nginx



## 安装



### docker

```shell
docker pull nginx # latest

DOCKER_NAME=nginx
# EXTERNAL_PORT=80	
# 多端口配置
EXTERNAL_PORT1=80
EXTERNAL_PORT2=81

# 创建挂载目录
mkdir -p /data/$DOCKER_NAME
mkdir -p /data/$DOCKER_NAME/www
mkdir -p /data/$DOCKER_NAME/conf.d

# 编写配置文件
vi /data/$DOCKER_NAME/nginx.conf

# 编写项目配置文件
vi /data/$DOCKER_NAME/conf.d/$PROJECT_NAME.conf

# 运行容器
# docker search nginx
docker run --name $DOCKER_NAME \
-p $EXTERNAL_PORT1:$EXTERNAL_PORT1 \
-p $EXTERNAL_PORT2:$EXTERNAL_PORT2 \
--restart=always \
-v /etc/localtime:/etc/localtime \
-v /data/$DOCKER_NAME/nginx.conf:/etc/nginx/nginx.conf \
-v /data/$DOCKER_NAME/conf.d:/etc/nginx/conf.d \
-v /data/$DOCKER_NAME/www:/usr/share/nginx/html \
-v /data/$DOCKER_NAME/logs:/var/log/nginx \
-d nginx

# 重载配置
docker exec -it $DOCKER_NAME nginx -s reload
```



### yum

```shell
# 配置yum源 centos 7
vim /etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/7/$basearch/
gpgcheck=0
enabled=1

# 安装
yum -y install nginx

# 自启
systemctl enable nginx

# 启动
systemctl start nginx
```



### make

```shell
# 下载源码文件
# 选择Stable version（稳定版）
http://nginx.org/en/download.html

# configure
cd /tmp/nginx-1.18.0
chmod +x configure \
	--prefix=/tmp/nginx/ \
	--sbin-path=
	


# configure params 
--prefix=path        # 指定安装目录
--sbin-path=path     # 默认可执行文件的路径。
--modules-path=path  # 定义一个将安装nginx动态模块的目录
--conf-path=path     # 设置nginx.conf配置文件的名称，可以使用-c 选项指定nginx.conf启动nginx.
--http-log-path=path  # 访问日志的路径。
--error-log-path=path  #  错误日志的路径。
--pid-path=path        # 设置将存储主进程的进程ID的nginx.pid文件的名称
--lock-path=path       # 设置lock文件的路径。
--user=username        # 设置其凭据将由工作进程使用的非特权用户的名称
--group=groupname      # 设置工作进程将使用其凭据的组的名称
 
--with-select_module    # 启用构建允许服务器使用该模块的模块
--without-select_module # 禁用构建允许服务器使用该模块的模块

--with-poll_module      启用构建允许服务器使用该模块的模块
--without-poll_module   禁用构建允许服务器使用该模块的模块

--with-threads  可以使用线程池。
--with-file-aio  启用异步文件I/O(AIO)
```



------

## 配置文件

### http block

```shell
# nginx 配置文件
user  nginx; # 进程用户
worker_processes  1; # 工作进程数，按照cpu核心数调整（4 core, 4）
error_log  /var/log/nginx/error.log warn; # 错误日志
pid        /var/run/nginx.pid; # pid文件路径

events {
    worker_connections  2048; # 最大连接数
}


http {
		# 加载子配置文件
    include       /etc/nginx/mime.types;
    include /etc/nginx/conf.d/*.conf;
    
    # nginx 默认文件类型
    default_type  application/octet-stream;

		# 日志格式化处理
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';
		
		# 连接日志
    access_log  /var/log/nginx/access.log  main;

		# sendfile是个比 read 和 write 更高性能的系统接口, 不过需要注意的是，sendfile 是将 in_fd 的内容发送到 out_fd 。而in_fd 不能是 socket, 也就是只能文件句柄。
		# 作为静态服务器时开启，可提高性能
		# 作为代理服务器时关闭
    sendfile        on;
    
    # 需同sendfile配置使用
    tcp_nopush     on; 
		
		# 指定tcp连接最多保持时间
    keepalive_timeout  65;
    
    # 客户端建立tcp连接后发送 request_body 超时时间 （HTTP 408（Request Timed Out））
    client_body_timeout 20s;
    
    # 客户端建立tcp连接后发送 request header 的超时时间
    client_header_timeout 10s;
    
    # 服务端向客户端传输数据的超时时间。
    send_timeout 30s;
    
    # 限流
    limit_req_zone 100;# 用来限制单位时间内的请求数，即速率限制,采用的漏桶算法 "leaky bucket"
   
		# 静态文件压缩
    gzip on|off; #是否开启gzip
    gzip_buffers 32 4K| 16 8K; #缓冲(压缩在内存中缓冲几块? 每块多大?)
    gzip_comp_level [1-9] #推荐6 压缩级别(级别越高,压的越小,越浪费CPU计算资源)
    gzip_disable #正则匹配UA 什么样的Uri不进行gzip
    gzip_min_length 200 # 开始压缩的最小长度(再小就不要压缩了,意义不在)
    gzip_http_version 1.0|1.1 # 开始压缩的http协议版本(可以不设置,目前几乎全是1.1协议)
    gzip_proxied # 设置请求者代理服务器,该如何缓存内容
    gzip_types text/plain application/xml # 对哪些类型的文件用压缩 如txt,xml,html ,css
    gzip_vary on|off # 是否传输gzip压缩标志
}
```



### server block

```nginx
server {
  listen 80;  										# 监听端口为80，可以自定义其他端口，也可以加上IP地址，如，listen 127.0.0.1:8080
  server_name localhost; 					# 定义网站域名，可以写多个，用空格分隔
	
  # https 相关配置，如果是通配符域名，可以单独创建域名ssl配置通过include引入
  listen 443 ssl;  								# 监听443端口，即ssl
  ssl_certificate cert.pem; 			# 指定pem文件路径
  ssl_certificate_key  cert.key;  # 指定key文件路径
  ssl_session_cache    shared:SSL:1m; 			# 指定session cache大小
  ssl_session_timeout  5m;  								# 指定session超时时间
  ssl_protocols TLSv1 TLSv1.1 TLSv1.2;   		# 指定ssl协议
	ssl_ciphers  HIGH:!aNULL:!MD5;  					# 指定ssl算法
	ssl_prefer_server_ciphers  on;  					# 优先采取服务器算法
 
  charset utf-8; 														# 定义网站的字符集
  access_log  logs/host.access.log  main; 	# 定义访问日志
 	error_page  404 /404.html; 								# 定义404页面
  error_page  500 502 503 504  /50x.html; 	# 当状态码为500、502、503、504时，则访问50x.html
  location = /50x.html {
        root /www/html; 										# 定义50x.html所在路径
  }
}

```

### location block

- 匹配顺序：精确匹配  > 以xx开头匹配 > 然后是按文件中顺序的正则匹配 > 通用匹配
- 位于server区块
- 如果location 和 if 同级，按照先后顺序执行

```nginx
# 静态页
location /test {
  # 定义网站根目录，目录可以是相对路径也可以是绝对路径,
  root /data/wwwroot/;  			#root路径＋location路径(/data/wwwroot/test)
  # 定义站点的默认页。
  index index.html index.htm;	
}

# 静态页于location 路径不同时
location /test {
  alias /data/wwwroot/; 
}

# 代理
location /server {
  proxy_pass http://127.0.0.1:8080;
  proxy_set_header Host $host:$server_port;    # 思路：通过/，将所有的请求，转发给第3方处理
}

# Redirect 跳转
location /index/ {
  rewrite ^(.*) http://127.0.0.1$1 redirect;
}

# 防盗链
location ~* \.(gif|jpg|png|bmp)$ {
  valid_referers none blocked *.ttlsa.com server_names	 ~\.baidu\.;
  if ($invalid_referer) {
    return 403;
    #rewrite ^/ http://www.ttlsa.com/403.jpg;
  }
}

# 禁止访问某个目录
location ~* \.(txt|doc)${
  root /data/www/wwwroot/linuxtone/test;  #所有用户都禁止访问这个目录
  deny all;
}

# 目录浏览功能
location /download/{
  autoindex on; # on: 开启目录浏览功能, off: 关闭目录浏览功能
  autoindex_localtime on; # on: 显示的文件时间为文件的服务器时间, off: 显示的文件时间为GMT时间
  autoindex_exact_size off; # on: 显示出文件的确切大小，单位是bytes, off: 显示出文件的大概大小，单位是kB或者MB或者GB
}

```

### upstream block

- 位于http区块

```nginx
upstream myservers {
  # ip_hash; # 访问ip的hash结果分配，这样每个访客固定访问一个后端服务器，可以解决session的问题。
  server 10.0.6.108:7080 weight=5; 
  server 10.0.0.85:8980 weight=10; 
}

location / { 
  root  html; 
  index  index.html index.htm; 
  proxy_pass http://linuxidc; 
}
```



------

### 常用配置

#### 80跳转443

```nginx
server {
  listen 80;
  location / {
    rewrite ^ https://$http_host$request_uri? permanent;
  }
}

server {
  listen 443 ssl;
  ssl_certificate     xxx.pem;
  ssl_certificate_key xxx.key;
  root xxx;
  location / {
    index index.html;
  }
}
```



#### 根据请求头做转发

```nginx
# 根据user—agent参数跳转移动端页面
location / {
  set $is_mobile 0;
  if ($http_user_agent ~* (mobile|nokia|iphone|ipad|android|samsung|htc|blackberry)) {
    # 设置变量is_mobile
    set $is_mobile 1;
  }
  # PC端
  if ($is_mobile = 0) {
    proxy_pass http://localhost:81;
  }
  # 移动端
  if ($is_mobile = 1) {
    proxy_pass http://www.195440.com;
  }
}
```



#### 配置php解析

```nginx
location ~ \.php$ {
  # fastcgi_pass   127.0.0.1:9000; # 配置php-fpm端口
  fastcgi_pass	unix:/home/leimengyao/php7/var/run/php-fpm.sock;# 配置php-fpm socket文件
  fastcgi_index	index.php;
  fastcgi_param   PATH_INFO   $fastcgi_path_info;
  fastcgi_param   SCRIPT_FILENAME $document_root$fastcgi_script_name;
}
```



### 常用变量

。。。待续

------

## 常见错误

### 403

> 无权限

```shell
# 检查nginx.conf 配置
user nginx;

# 检查root目录权限
server {

# 修改所属chown nginx:nginx -R $DIR
	root /www/www.demo.com;
}

# 检查index文件权限(是否存在)
location {
	index index.html index.htm;
}

# 检查SELINUX是否关闭
linux> setenforce 0
```



### 404