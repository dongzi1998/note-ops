# Jenkins 使用笔记



## 一、安装

### Docker

```shell
DOCKER_NAME=jenkins

# 创建挂载目录
mkdir /data/jenkins
chown -R 1000:1000 !$
	
docker pull jenkins
docker run --name $DOCKER_NAME \
--restart always \
--privileged true \
-u root \
-p 8000:8080 \
-p 50000:50000 \
-v /data/$DOCKER_NAME:/var/jenkins_home  \
-d jenkins

# 修改代理地址，加速插件下载
sed -i 's/http:\/\/updates.jenkins-ci.org\/download\\/https:\/\/mirrors.tuna.tsinghua.edu.cn\/jenkins\\/g' /data/jenkins/updates/default.json
sed -i 's/http:\/\/www.google.com/http:\/\/www.baidu.com/g' /data/jenkins/updates/default.json


```



### Yum

### Makefile



## 二、常用插件

>汉化： Locale plugin、Localization: Chinese (Simplified)
>
>ssh支持：SSH Credentials、SSH Build Agents、Publish Over SSH
>
>自动安装maven 版本  ：Maven Integration
>
>git 支持： Git Parameter、Git client、Git 、Git server 
>
>权限管理：Role-based Authorization Strategy
>
>文件夹管理： Folders
>
>特殊依赖：JSch dependency



## 三、通知

### 1、钉钉通知（待封装）







