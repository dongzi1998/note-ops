

# 	Prometheus



## 一、安装

### Docker

```shell
docker pull prom/prometheus
docker run -d -p 9100:9100 \
-v "/proc:/host/proc:ro" \
-v "/sys:/host/sys:ro" \
-v "/:/rootfs:ro" \
--net="host" \
prom/node-exporter
```



### Binary

1、下载地址：https://github.com/prometheus/prometheus/releases/download/v2.20.1/prometheus-2.20.1.linux-amd64.tar.gz

```shell
# 二进制
tar -zxvf prometheus-2.20.1.linux-amd64.tar.gz
cd  prometheus-2.20.1.linux-amd64
nohup ./prometheus & 
```



## 二、使用

### 配置

```yml
# prometheus.yml
scrape_configs:
  - job_name: 'prometheus'
    static_configs:
      - targets: ['localhost:9090']
  # 采集node exporter监控数据
  - job_name: 'node'
    static_configs:
      - targets: ['localhost:9100']
      
rule_files:
	- filepath_glob: group_example.yml
      
# group_example.yml 
groups:
- name: example
  rules:
  - alert: HighErrorRate # 告警规则的名称
    expr: job:request_latency_seconds:mean5m{job="myjob"} > 0.5 # 基于PromQL表达式告警触发条件，用于计算是否有时间序列满足该条件
    for: 10m	# 当触发条件持续一段时间
    labels:		# 自定义标签	
      severity: page
    annotations: # 定义附加信息
      summary: High request latency
      description: description info
```



### PromQL

```shell
# 常用

# 获取内存使用率
100-(node_memory_MemFree_bytes+node_memory_Buffers_bytes+node_memory_Cached_bytes)/node_memory_MemTotal_bytes*100

# 获取磁盘使用百分比
(node_filesystem_size_bytes - node_filesystem_free_bytes) / node_filesystem_size_byte 
```

### 



