# 	prometheus exports 



## Mysql

- MySQL >= 5.6.
- MariaDB >= 10.2

#### 创建监控用户

```sql
-- 创建用户：限制连接数
CREATE USER 'exporter'@'localhost' IDENTIFIED BY 'XXXXXXXX' WITH MAX_USER_CONNECTIONS 3;
-- 授权用户：进程、复制账户、查看
GRANT PROCESS, REPLICATsION CLIENT, SELECT ON *.* TO 'exporter'@'localhost';
```

### Docker

```shell
data_source_name="user:password@(hostname:3306)/"

# docker network create my-mysql-network
docker pull prom/mysqld-exporter
docker run -d \
  -p 9104:9104 \
  --network my-mysql-network  \
  -e DATA_SOURCE_NAME=$data_source_name \
  prom/mysqld-exporter
```



## Redis



## RabbitMQ



## ES（elasticsearch）

​	