# Zabbix Server

> 官网地址：https://www.zabbix.com/



## 搭建

#### Yum

```shell
# 安装Zabbix存储库(5.0 TSL)
rpm -Uvh https://repo.zabbix.com/zabbix/5.0/rhel/7/x86_64/zabbix-release-5.0-1.el7.noarch.rpm
yum clean all
# 安装Zabbix服务器和代理
yum install zabbix-server-mysql zabbix-agent
# 启用Red Hat软件集合
yum install centos-release-scl
# 编辑配置文件 
vim /etc/yum.repos.d/zabbix.repo
[zabbix-frontend]
...
enabled=1
...
# 安装Zabbix前端软件包。
yum install zabbix-web-mysql-scl zabbix-nginx-conf-scl

# 创建初始数据库
mysql -uroot -p
create database zabbix character set utf8 collate utf8_bin;
create user zabbix@'%' identified by 'password';
grant all privileges on zabbix.* to zabbix@'%';
quit;

# 导入初始表结构和数据，系统将提示您输入新创建的密码。
zcat /usr/share/doc/zabbix-server-mysql*/create.sql.gz | mysql -uzabbix -p zabbix

# 为Zabbix server配置数据库
vim /etc/zabbix/zabbix_server.conf
DBPassword=password

# 为Zabbix前端配置PHP
vim /etc/opt/rh/rh-nginx116/nginx/conf.d/zabbix.conf
# listen 80;
# server_name example.com;

# 编辑配置文件
vim  /etc/opt/rh/rh-php72/php-fpm.d/zabbix.conf
listen.acl_users = apache,nginx

# 设置时区
; php_value[date.timezone] = Europe/Riga

# 启动Zabbix server和agent进程
systemctl restart zabbix-server zabbix-agent rh-nginx116-nginx rh-php72-php-fpm
systemctl enable zabbix-server zabbix-agent rh-nginx116-nginx rh-php72-php-fpm

```



