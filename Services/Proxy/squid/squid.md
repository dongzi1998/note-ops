# squid 代理服务器（centos7）

## 一、安装

### Yum

```shell
# install
  yum -y install squid
# start 
systemctl start squid
```



------

## 二、配置

`/etc/squid/squid.conf`

```shell
# 最底部增加
http_access allow all # 访问控制
# http_access deny all # 注释掉
# 其他
http_port 3128 # 端口配置，默认端口3128
access_log /var/log/squid/access.log # 访问日志路径
acl OverConnLimit maxconn 16 # 限制每个IP最大允许16个连接，防止攻击
```

`账户配置`

```shell
htpasswd -c /etc/squid/passwd proxy_username # 同时生成passwd文件
# htpasswd proxy_username
systemctl restart squid # 重启服务
```

