# Haproxy



## 一、安装

### Docker

```shell
haproxy_path=/Users/hedongdong/Desktop/haproxy_data
haproxy_conf_path=$haproxy_path/etc/

mkdir -p $haproxy_conf_path
vim $haproxy_conf_path/haproxy.cfg

docker pull haproxy:2.0-alpine
docker run -d --name haproxy \
--network=host \
-v $haproxy_conf_path:/usr/local/etc/haproxy \
haproxy:2.0-alpine
```





## 二、配置文件

haproxy.cfg

```shell
# 全局配置
global
	# 启用后台模式
  daemon
  # 最大并发连接数
  maxconn 1000

# 默认配置
defaults
  mode http
  # 连接超时时间
  timeout connect 5000ms
  # 客户端超时时间
  timeout client 50000ms
  # 服务器超时时间
  timeout server 50000ms


frontend http-in
	# 暴露端口
  bind *:80
  default_backend web_servers

backend web_servers
	# 后台服务
	server server1 127.0.0.1:8000 maxconn 32
```











