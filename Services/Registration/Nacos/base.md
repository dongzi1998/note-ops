# Nacos



## 安装

```shell
DOCKER_NAME=nacos-test

docker pull nacos/nacos-server:1.4.0

# 连接数据库初始化用户
mysql -uroot -ppBHmjfc7Aes35wkS5L -h 192.168.0.197 -P 3307
create database exam_nacos charset utf8;	
create user 'exam_nacos'@'%' identified by 'gWDjuV4vbY9XqcLgm6';
grant all on exam_nacos.* to exam_nacos;

# 执行nacos_init.sql
mysql -uexam_nacos -pgWDjuV4vbY9XqcLgm6 -h 127.0.0.1 -P 3308 --database exam_nacos < /data/exam-mysql-test/sql/nacos-mysql.sql	
mysql -uexam_nacos -pgWDjuV4vbY9XqcLgm6 -h 127.0.0.1 -P 3308 --database exam_nacos < /data/exam-mysql-test/sql/schema.sql

docker run --name $DOCKER_NAME  \
--restart=always \
-e PREFER_HOST_MODE=hostname \
-e MODE=standalone \
-p 8848:8848 \
-v /etc/localtime:/etc/localtime \
-e MODE=standalone \
-e PREFER_HOST_MODE=hostname \
-e SPRING_DATASOURCE_PLATFORM=mysql \
-e MYSQL_SERVICE_HOST=192.168.0.197 \
-e MYSQL_SERVICE_PORT=3307 \
-e MYSQL_SERVICE_USER=exam_nacos \
-e MYSQL_SERVICE_PASSWORD=gWDjuV4vbY9XqcLgm6 \
-e MYSQL_SERVICE_DB_NAME=exam_nacos \
-d nacos/nacos-server:1.4.0
# 账户密码：nacos 4RJiVfKLsBhZUjck2G
```



## 配置

```yaml
server.contextPath=/nacos
server.servlet.contextPath=/nacos
server.port=8848
spring.datasource.platform=mysql
db.num=1
db.url.0=jdbc:mysql://172.17.118.48:3306/nacos?characterEncoding=utf8&connectTimeout=1000&socketTimeout=3000&autoReconnect=true
db.user=root
db.password=6spEvePSWP5DpUzh
nacos.cmdb.dumpTaskInterval=3600
nacos.cmdb.eventTaskInterval=10
nacos.cmdb.labelTaskInterval=300
nacos.cmdb.loadDataAtStart=false
management.metrics.export.elastic.enabled=false
management.metrics.export.influx.enabled=false
server.tomcat.accesslog.enabled=true
server.tomcat.accesslog.pattern=%h %l %u %t "%r" %s %b %D %{User-Agent}i
nacos.security.ignore.urls=/,/**/*.css,/**/*.js,/**/*.html,/**/*.map,/**/*.svg,/**/*.png,/**/*.ico,/console-fe/public/**,/v1/auth/login,/v1/console/health/**,/v1/cs/**,/v1/ns/**,/v1/cmdb/**,/actuator/**,/v1/console/server/**
nacos.naming.distro.taskDispatchThreadCount=1
nacos.naming.distro.taskDispatchPeriod=200
nacos.naming.distro.batchSyncKeyCount=1000
nacos.naming.distro.initDataRatio=0.9
nacos.naming.distro.syncRetryDelay=5000
nacos.naming.data.warmup=true
nacos.naming.expireInstance=true
```

