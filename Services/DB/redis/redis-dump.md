# redis-dump



## 一、安装

### docker

```shell
docker pull gaofeng0300/redis-dump:latest
DOCKER_NAME=redis-dump
mkdir -p /data/$DOCKER_NAME/data

```



## 二、使用

### 备份

```shell
# 导出
# 安装redis-dump
docker pull gaofeng0300/redis-dump:latest

mkdir -p /data/my-redis-dump/data
cd /data/my-redis-dump/
rz
docker run --rm --name redis-dump \
-v /data/my-redis-dump/data:/data \
-v /data/my-redis-dump/dump.rb:/var/lib/gems/2.5.0/gems/redis-dump-0.4.0/lib/redis/dump.rb \
-d gaofeng0300/redis-dump:latest

# 导出
docker exec -it redis-dump bash
redis-dump -u dAZUaBHhpEBDsybastcN@192.168.0.236:6380 > /data/redis_prod.json
exit
sz /data/my-redis-dump/data/redis_prod.json
```



## 

```shell
# 导入
# 安装redis-dump
rz
mv redis_prod.json /data/my-redis-dump/data/
docker exec -it redis-dump bash
cat redis_prod.json | redis-load -u :K5WgT3T6S3FbbtmyK6@172.30.238.50:6379
```

