# Redis



## 一、安装

### Docker

```shell
DOCKER_NAME=redis

# 创建挂载目录
mkdir -p /data/$DOCKER_NAME/conf
mkdir -p /data/$DOCKER_NAME/data

# 编写配置文件
vi /data/$DOCKER_NAME/conf/redis.conf

# 运行
docker pull redis
docker run --name $DOCKER_NAME \
--restart=always \
-p 6379:6379 \
-v /data/$DOCKER_NAME/conf/redis.conf:/etc/redis/redis.conf \
-v /data/$DOCKER_NAME/data:/data \
-d redis redis-server \
/etc/redis/redis.conf 

# 验证
redis-cli -h 127.0.0.1 -p 6381
```



### Yum



### MakeFile



## 二、配置文件

### redis.conf

```shell
bind 127.0.0.1 		# 限制redis只能本地访问
daemonize no			# 默认no，改为yes意为以守护进程方式启动，可后台运行，除非kill进程，改为yes会使配置文件方式启动redis失败
databases 16 			# 数据库个数（可选）
protected-mode no #	默认yes，开启保护模式，限制为本地访问
dir /data 				#	输入本地redis数据库存放文件夹（可选）

maxclients 10000	# 最大连接数

# 集群配置
cluster-enabled yes


# 主从配置
slave-announce-ip 127.0.0.1							# 自定义slave ip
slave-announce-port 6380 | 6381 | 6382	# 自定义slave port


appendfsync always 		# 同步刷新 AOF 文件
appendfsync everysec	# 每秒同步刷新 AOF 文件
appendonly yes 				# redis持久化（可选）
appendfilename appendonly.aof #开启持久化后的文件名

notify-keyspace-events "Ex" # 开启过期事件通知
requirepass 1234 						# 密码
logfile "redis.log" 				# 日志名称
port 6379 									# 端口

```



## 三、使用



## 四、备份 & 恢复

### 备份

> 开启AOF

### 恢复

```shell
server_ip=127.0.0.1
server_port=6379
aof_path=/back/...aof

./redis-cli -h $server_ip -p $server_port --pipe < $aof_path
```

