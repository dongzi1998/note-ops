# Mysql 主从

## 一、原理

`master - slave`

> 主服务器上的任何修改操作存入Binary log里
>
> 从服务器启动I/O theard ，从主服务器读取二进制日志，将读取到日志写入本地Realy log
>
> 从服务器开启SQL theard 定时检查 Realy log ，如有更改则执行更改内容

`master - slave - slave ...`

>选取一个从服务器进行二进制分发给其他从服务器，不进行Write realy log操作，节省开支

## 二、配置

### 1、准备操作

> Mysql server 安装，版本一致
>
> 表初始化，同步

### 2、主服务器操作

`/etc/my.cnf`

```shell
[mysqld]
log-bin=binlog  // 启动二进制日志
server-id=100 			//服务器唯一ID，默认值是1，一般设置为IP地址的最后一段数字
```

> 修改好配置，重启服务生效配置

`创建同步账户并授权`

``` mysql
grant replication slave on *.* to '用户名'@'%' identified by '任意密码';
# 查看 master 状态（bin-log开启状态）
show master status; 
```

### 3、从服务器操作

```mysql
change master to \
master_host='MySQL主服务器IP地址',\
master_user='之前在MySQL主服务器上面创建的用户名',\
master_password='之前创建的密码',\
master_log_file='MySQL主服务器状态中的二进制文件名(binlog)',\
master_log_pos='MySQL主服务器状态中的position值';
# 启用slave
reset slave;
start slave;
SET GLOBAL SQL_SLAVE_SKIP_COUNTER=1; START SLAVE;
# 查看MySQL从服务器的状态
show slave status\G
```

> Slave_IO_Running: Yes   此状态必须YES
>
> Slave_SQL_Running: Yes    此状态必须YES

```shell
  change master to \
  master_host='192.168.0.176',\
  master_user='root',\
  master_port=3308,\
  master_password='67jZTaQz4KFGmDkaPE',\
  master_log_file='/var/log/mysql/bin-log.log',\
  master_log_pos=154;
```

