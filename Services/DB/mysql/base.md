# Mysql

## 一、安装

### Docker

```shell
DOCKER_NAME=mysql
MYSQL_ROOT_PASSWORD=78Jikbfz6zKYfPjC
# 创建挂载目录
mkdir -p /data/$DOCKER_NAME
mkdir -p /data/$DOCKER_NAME/conf
mkdir -p /data/$DOCKER_NAME/data
mkdir -p  /data/$DOCKER_NAME/sql

# 编写初始化sql
vi /data/$DOCKER_NAME/sql/init.sql

# 编写mysql配置文件
vi /data/$DOCKER_NAME/conf/my.cnf

# 下载容器镜像
# docker search mysql
docker pull mysql:5.7

# 运行容器
docker run --name $DOCKER_NAME \
--restart=always \
-p 3306:3306 \
-v /data/$DOCKER_NAME/conf:/etc/mysql \
-v /data/$DOCKER_NAME/data:/var/lib/mysql \
-e MYSQL_ROOT_PASSWORD=$MYSQL_ROOT_PASSWORD \
-d mysql:5.7

# 执行初始化脚本
mysql -uroot -p$MYSQL_ROOT_PASSWORD -h 127.0.0.1 -P 3306 < /data/$DOCKER_NAME/sql/init.sql

# 登陆验证
mysql -uroot -p$MYSQL_ROOT_PASSWORD -h 127.0.0.1 -P 33307
mysql> show databases ;
```



### MakeFile



### Yum安装



## 二、配置文件

```shell
# 客户端登录配置
[client]
port = 3306 # 端口号
socket = /var/lib/mysql/mysql.sock # 套接字文件

# 客户端命令行配置
[mysql]
no-auto-rehash # 默认不自动补全 auto-rehash自动补全

# 服务优化配置
[mysqld]
skip-grant-tables # 跳过登录验证
user = mysql # 默认启动用户，一般不需要修改，可能出现启动不成功
port = 3306 # 端口号
socket = /var/lib/mysql/mysql.sock # 套接字文件 （套接字方式登陆比TCP/IP方式连接快）
character-set-server = utf8mb4 # 设置数据库服务器默认编码 utf-8
basedir = /usr/local/mysql # 数据库安装目录--指定此参数可解决相对路径造成的问题
datadir = /var/lib/mysql #数据库目录，数据库目录切换时需要用到
pid-file = /var/run/mysqld/mysqld.pid #mysql进程文件，可指定自己的进程文件
external-locking = FALSE #外部锁定(非多服务器可不设置该选项，默认skip-external-locking)
skip-external-locking #跳过外部锁定 （避免多进程环境下的external locking--锁机制）
skip-name-resolve = 1 #跳过主机名解析，直接IP访问，可提升访问速度
log-error = /data/log/mysqld_error.log #错误日志文件

# 重要配置
max_connections = 5000 # 最大连接数
max_connect_errors = 6000 # 客户端请求异常中断次数
max_allowed_packet = 32M # 限制单条数据大小
sort_buffer_size = 8M # 每个连接独享内存数，如：500连接 * 8 = 4G 内存
join_buffer_size = 8M # 表关联缓存大小，每个连接独享

# 数据库引擎相关参数
default-storage-engine = InnoDB # 默认数据库引擎

# 性能分析
slow-query-log = 1 # 是否记录慢查询日志
long_query_time = 2 # 慢查询超时时间设置
slow-query-log-file=/var/log/mysql/query-slow.log #慢查询日志记录文件

# 性能优化
back_log=500 # 堆栈等待连接数
innodb_autoinc_lock_mode=2|0|1 # 0：每次都会产生表锁，1：会产生一个轻量锁，2：不会锁表，并发最高
net_write_timeout=120 # 等待将一个block发送给客户端的超时时间，默认为60s
tmp_table_size=128M # 默认为16M，可调到64-256最佳，线程独占，太大可能内存不够I/O堵塞
loose_rds_max_tmp_disk_space=10737418240


# 二进制文件设置
log_bin = /data/log/mysql/bin-log.log # 开启bin-log日志，指定存储路径
binlog_format = ROW # ROW(基于行的复制--安全，但是注意并发) STATEMENT(基于sql语句的复制)，MIXED(混合模式复制)
binlog_cache_size = 4M # 二进制日志缓存，提高log-bin记录效率
log_bin_trust_function_creators = 1 #主从复制是需要注意，为了保证主从复制完全一致，需要开启此选项，主从默认阻止函数创建
max_binlog_size = 1G # 二进制日志文件大小默认1G 要求大于4096 小于1G
expire_logs_days = 7 # 清除过期日志

# 主从复制相关
server-id = 2020 #主从复制必须，并且各服务器具有唯一性
log_slave_updates #配置从服务器的更新是否写入二进制日志，默认是不打开的
replicate-ignore-db = mysql #主从复制默认忽略的数据库，可用","分隔或使用多条记录
# replicate-do-db=qrs,login #主从复制指定数据库,","号隔开或使用多条记录

#数据库全量备份
[mysqldump] 
quick #强制mysqldump从服务器一次一行地检索表中的行
max_allowed_packet = 32M #可接收数据包大小

#在mysqld服务器不使用的情况下修复表或在崩溃状态下恢复表
[isamchk] 
key_buffer = 1024M
sort_buff_size =1024M
read_buffer = 16M
write_buffer = 16M


[mysqld_safe] #safe方式启动数据库，相比于mysqld,会在服务启动后继续监控服务状态，死机时重启
open-files-limit = 8192

```



## 三、备份

### mysqldump

```shell
#!/bin/bash

# 建造AR-mysql
server_host=
server_port=
server_user=
server_passwd=
back_date=`date "+%Y-%m-%d"`
back_time=`date "+%H:%M:%S"`
back_path=/data/backup/$back_date/mysql

# 切换到备份目录
mkdir -p $back_path
cd $back_path


# db列表(过滤不备份库)
db_list=`mysql -h $server_host \
-P $server_port \
-u$server_user \
-p$server_passwd \
-e "show databases;" \
| grep -Ev "Database|information_schema|mysql|test|performance_schema|sys"
`

# 遍历备份
for db in $db_list;do
sql_name=${db}_${back_time}.sql
mysqldump -h $server_host \
-P $server_port \
-u$server_user \
-p$server_passwd \
--databases $db > $sql_name
done

# 删除过期文件
find /data/backup/ -mindepth 2 -type d -mtime +3 -exec rm -rf {} \;
```

```shell
--all-databases \ # 备份所有服务器
--compact \ # 压缩模式
--comments \ # 添加注释信息
--complete-insert \ # 输出完成的插入语句
--lock-tables \ # 备份前，锁定所有数据库表
--no-create-db | --no-create-info \ # 禁止生成创建数据库语句
--force \ # 当出现错误时仍然继续备份操作
--default-character-set \ # 指定默认字符集
--add-locks 
--no-data \ # 不到处
```



## 四、根据BinLog恢复

```shell
# 开启binlog

# 
```





## 常用操作

### 更新root密码

```sql
update user set password=password('123') where user='root' and host='localhost'; 
```

