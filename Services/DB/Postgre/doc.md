# Postgre



## 安装

```bash
docker run \
--name postgres \
-e POSTGRES_PASSWORD=123123123 \
-e PGDATA=/var/lib/postgresql/pgdata/data \
-v /data/postgres:/var/lib/postgresql/pgdata \
-p 5432:5432 \
-d postgres
```



## pgAdmin

```bash
docker run -p 80:80 \
    -e 'PGADMIN_DEFAULT_EMAIL=user@domain.com' \
    -e 'PGADMIN_DEFAULT_PASSWORD=SuperSecret' \
    -e 'PGADMIN_CONFIG_ENHANCED_COOKIE_PROTECTION=True' \
    -e 'PGADMIN_CONFIG_LOGIN_BANNER="Authorised users only!"' \
    -e 'PGADMIN_CONFIG_CONSOLE_LOG_LEVEL=10' \
    -d dpage/pgadmin4
```



## 备份 & 恢复

```shell
# 备份

# 恢复
psql -h localhost \ 
-U postgres \
-d databasename \
<  back.dump
```

