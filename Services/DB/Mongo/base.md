# MongoDB



## Install

### Docker

```shell
 docker pull mongo:latest
 docker run -itd --name mongo \
 -p 27017:27017 \
 mongo \
 --auth
 
 # --auth 开启密码认证
```



### BinaryFile

> https://www.mongodb.com/download-center#community



------

## Setting

### Create User

```shell
# Enter the mongo terminal
mongo admin

# create user 'admin' and set password '123123'
# set role userAdminAnyDatabase
# set authority readWriteAnyDatabase
db.createUser({ user:'admin',pwd:'123123',roles:[ { role:'userAdminAnyDatabase', db:'admin'},"readWriteAnyDatabase"]});

# Test connect
db.auth('admin', '123456')
```

