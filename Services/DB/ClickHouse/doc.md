# ClickHouse



## 安装

### Docker

```shell
docker_name=clickhouse
docker_image=yandex/clickhouse-server

mkdir -p /data/$docker_name/etc/
cd /data/$docker_name
rz clickhouse_etc.tar.gz

docker pull $docker_image
docker run -d \
--name $docker_name \
--ulimit nofile=262144:262144 \
-p 9000:9000 \
-p 8123:8123 \
-v /data/$docker_name/etc/:/etc/clickhouse-server/ \
$docker_image
```



## 配置

### 用户配置

#### 新增用户

```

```

