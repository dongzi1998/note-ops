# Bitwardenrs



## 搭建

### docker

```shell
DOCKER_NAME=bitwardenrs
ADMIN_TOKEN=oQvzsxNjMhZxoKzZ

docker pull bitwardenrs/server:latest

mkdir -p /data/$DOCKER_NAME/data

docker run --name bitwarden \
-d \
-v /data/$DOCKER_NAME/data:/data/ \
-p 6666:80 \
-e SIGNUPS_ALLOWED=false \
-e INVITATIONS_ALLOWED=false \
-e ADMIN_TOKEN=$ADMIN_TOKEN \
bitwardenrs/server:latest
```

```shell
# 其他参数
-e SIGNUPS_ALLOWED=false \
-e INVITATIONS_ALLOWED=false \
-e ADMIN_TOKEN=step2_generated_token \
-e ROCKET_TLS='{certs="/data/v2ray.crt",key="/data/v2ray.key"}' \
-e DOMAIN=https://bwh.vioe.cc/ \
-e LOG_FILE=/path/to/log \
-e LOG_LEVEL=warn -e EXTENDED_LOGGING=true \
-e DATA_FOLDER=/path/to/data/folder \
-p 443:80 \
-v /path/to/host/ssl/:/path/to/docker/ssl/
-v /path/to/host/data/folder:/path/to/docker/data/folder \

```

```yml
SIGNUP_ALLOWED: 是否允许注册
INVITATIONS_ALLOWED: 是否允许组织邀请注册
ADMIN_TOKEN: 用户管理界面 (/admin)，可用于删除用户及邀请用户注册
ROCKET_TLS: ssl 证书信息，同时需要配置 -v /path/to/host/ssl/:/path/to/docker/ssl/ 卷，前者为宿主机 ssl 证书的位置，后者为容器证书位置
DOMAIN: 域名
LOG_FILE、LOG_LEVEL、EXTENDED_LOGGING: 日志保存文件路径以及日志等级定义
DATA_FOLDER: docker 容器数据保存文件夹（默认为 /data），除了定义这个文件夹之外，还可以定义附件、图标缓存、数据库等参数
DATABASE_URL: 数据库路径
ATTACHMENT_FOLDER: 附件路径
ICON_CACHE_FOLDER: 图标缓存路径
```

