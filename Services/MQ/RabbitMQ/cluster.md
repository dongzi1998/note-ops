# Rabbitmq集群 笔记



## 默认模式

> 适合非持久化序列
>
> 可保证**单节点**崩溃情况下继续连接其他节点生产和消费，**但是**其他队列所有**消息**会**丢失**
>
> 至少三个节点、**一个disc节点**保证高可用，也可三个disc节点

#### 搭建

##### 单机

- port1: 

```shell
# 执行: 每台服务器
# 预研环境，数据不挂载
image_name=rabbitmq:3.8.0-management
rabbitmq_cookice="rabbitcookice"

docker pull $image_name

docker run -d --hostname rabbit1 \
--name rabbit1 \
-p 15672:15672 \
-p 5672:5672 \
-e RABBITMQ_ERLANG_COOKIE=$rabbitmq_cookice \
$image_name

docker run -d --hostname rabbit2 \
--name rabbit2 \
-p 5673:5672 \
--link rabbit1:rabbit1 \
-e RABBITMQ_ERLANG_COOKIE=$rabbitmq_cookice \
$image_name

docker run -d --hostname rabbit3 \
--name rabbit3 \
-p 5674:5672 \
--link rabbit1:rabbit1 \
--link rabbit2:rabbit2 \
-e RABBITMQ_ERLANG_COOKIE=$rabbitmq_cookice \
$image_name

docker exec -it rabbit2 bash 
# docker exec -it rabbit3 bash 
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl join_cluster rabbit@rabbit1 --ram
rabbitmqctl start_app	

mac > open http://127.0.0.1:15672

```



##### 多机

- server1：192.168.0.197  master
- server2：192.168.0.196  salve
- server3：192.168.0.195  salve

```shell
# 执行: 每台服务器
image_name=rabbitmq:3.8.0-management
docker pull $image_name

# 环境准备
docker_name=rabbit1|2|3
rabbitmq_path=/data/$docker_name/
rabbitmq_data_path=$rabbitmq_path/data
rabbitmq_cookice="rabbitcookice"
rabbitmq_user=admin
rabbitmq_pass=vbLWDjuV4Y9Xqcgm6g
mkdir -p $rabbitmq_data_path

# 设置hosts，用于加入集群
echo 192.168.0.197 rabbit1 >> /$rabbitmq_path/hosts
echo 192.168.0.196 rabbit2 >> /$rabbitmq_path/hosts
echo 192.168.0.195 rabbit3 >> /$rabbitmq_path/hosts

# 启动容器
docker run -d \
-u root \
--net host \
--hostname $docker_name \
--name $docker_name \
--restart=always \
-v $rabbitmq_path/hosts:/etc/hosts \
-v $rabbitmq_data_path:/var/lib/rabbitmq/mnesia \
-e RABBITMQ_ERLANG_COOKIE=$rabbitmq_cookice \
-e RABBITMQ_DEFAULT_USER=$rabbitmq_user \
-e RABBITMQ_DEFAULT_PASS=$rabbitmq_pass \
$image_name

# 执行：server2，server3
# rabbit1为主节点的hostname
# --ram 以内存节点加入集群
docker exec -it $docker_name bash 
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl join_cluster rabbit@rabbit1 
rabbitmqctl start_app	
```

##### rabbitmqctl

```shell
# 将故障节点踢出, 节点启动后不可加入集群（!将无法启动）
# 节点需停止后才可提出
rabbitmqctl -n rabbit@rabbit1  forget_cluster_node  rabbit@rabbit3

# 加入集群
join_cluster rabbit@rabbit1
```



#### 实验

##### 配置可用性

1. 搭建单机模式
2. 配置用户
3. 停止ram节点（用户存在），启动后（可自动加入集群）
4. 停止disc节点（web-ui无法访问），启动后（自动加入集群）

##### 消息可用性





## 主从模式（主备）

> **主节点**：进行读写，**挂了**后**从节点**升级为主节点，**恢复**后降级为从节点
>
> **从节点**：不进行读写，仅进行备份





## 镜像模式

TODO : 待完善



## 多活模式

TODO : 待完善



## 远程模式

TODO : 待完善

