# RabbitMQ 笔记

## 一、安装

### Docker

```shell
# rabbitmq_data=/data/rabbitmq/data
docker_name=rabbit
rabbitmq_path=/data/$docker_name/
rabbitmq_data_path=$rabbitmq_path/data
rabbitmq_cookice="rabbitcookice"
rabbitmq_user=admin
rabbitmq_pass=vbY9XqcLgm6gWDjuV4

# Version 3.8.0
docker pull  rabbitmq:3.8.0-management
docker run -d \
-u root \
-p $exposed_port
--hostname $docker_name \
--name $docker_name \
-v $rabbitmq_data_path:/var/lib/rabbitmq/mnesia \
-e RABBITMQ_ERLANG_COOKIE=$rabbitmq_cookice \
-e RABBITMQ_DEFAULT_USER=$rabbitmq_user \
-e RABBITMQ_DEFAULT_PASS=$rabbitmq_pass \
rabbitmq:3.8.1-management


```

### Yum

```shell
# TODO 待完善
```



## 二、使用



### 插件安装

###### rabbitmq_delayed_message_exchange

```shell
# 安装插件
# 注意：该插件仅支持disc模式下使用
docker cp rabbitmq_delayed_message_exchange-3.8.0.ez  $docker_name:/plugins
docker exec -it $docker_name rabbitmq-plugins enable rabbitmq_delayed_message_exchange
```





## 三、备份 & 恢复

#### 备份

```shell
# 元文件（queues，user，setting）
rabbitmq_ip=192.168.0.2
rabbitmq_port=15672
meta_data_save_path=./rabbit_source.json

wget \
--user root \
--password root \
http://$rabbitmq_ip:$rabbitmq_port/api/definitions \
-O $meta_data_save_path

# 消息（message）
# 确定数据目录
rabbitmq_mnesia_path=`rabbitmqctl eval 'rabbit_mnesia:dir().'`
rabbitmq_mnesia_back_name=rabbitmq_back.tar.gz

# 避免数据不一致，停止服务（数据全量恢复，不建议此操作）
# service rabbitmq-server stop
tar  -zcvf  $rabbitmq_mnesia_back_name  $rabbitmq_mnesia_path

```



#### 恢复

```

```

