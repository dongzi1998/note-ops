# NFS

## 一、安装

### Yum

```shell
yum install -y nfs-utils rpcbind

# 启动
systemctl enable rpcbind.service
systemctl enable nfs-server.service
systemctl start rpcbind.service
systemctl start nfs-server.service

# 检查 NFS 服务器是否挂载我们想共享的目录 /home/nfs/：
rpcinfo -p

# 使配置生效
exportfs -r


```



## 二、使用

### 配置

```shell
vim /etc/exports

# 同192.168.248.0/24一个网络号的主机可以挂载NFS服务器上的/data/nfs/目录到自己的文件系统中
# rw表示可读写；sync表示同步写，fsid=0表示将/data找个目录包装成根目录

/data/nfs/ 192.168.248.0/24(rw,sync,fsid=0)
```

### 客户端

```shell
yum install -y nfs-utils rpcbind

NFS_IP=
mkdir -p /data/nfs/

# 启动rpcbind
systemctl enable rpcbind.service
systemctl start rpcbind.service

# 检查 NFS 服务器端是否有目录共享
showmount -e nfs服务器的IP

# 挂载
mount -t nfs $NFS_IP:/data/nfs /data/nfs

# 查看是否挂载成功
df -h 

```

