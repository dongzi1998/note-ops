# minio

## 一、安装

### docker

````shell
docker pull minio/minio:latest

DOCKER_NAME=minio
MINIO_ACCESS_KEY=admin
MINIO_SECRET_KEY=topeduol

mkdir -p /data/$DOCKER_NAME/data
mkdir -p /data/$DOCKER_NAME/conf

docker run --name $DOCKER_NAME \
-p 9000:9000 \
-d \
-e "MINIO_ACCESS_KEY=$MINIO_ACCESS_KEY" \
-e "MINIO_SECRET_KEY=$MINIO_SECRET_KEY" \
-v /data/$DOCKER_NAME/data:/data \
-v /data/$DOCKER_NAME/conf:/root/.minio \
minio/minio server /data


# 客户端
docker pull minio/mc

docker run -it --entrypoint=/bin/sh minio/mc

mc config host add myminio http://192.168.0.1:9000 $MINIO_ACCESS_KEY  $MINIO_SECRET_KEY 

# 配置指定桶开放下载
mc policy set public myminio/backup


````

### 源码

...

------

## 二、mc客户端使用

### bucket桶策略

```json
cat > getonly.json << EOF
{
  "Version": "2012-10-17", 
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [												// 	可以做出的行动（权限）
        "s3:ListAllMyBuckets",          //  查看所有的“桶”列表
				"s3:ListBucket",               	//  查看桶内的对象列表
				"s3:GetBucketLocation",         
				"s3:GetObject",               	//  下载对象
				"s3:PutObject",               	//  上传对象
				"s3:DeleteObject"            	 	//  删除对象									
      ],
      "Resource": [
        "arn:aws:s3:::my-bucketname/*" 	// （应用到的资源，*表示所有，也可以用路径来控制范围。arn:aws:s3是命名空间）
      ],
      "Sid": ""
    }
  ]
}
EOF
```

```shell
# 添加该策略
mc admin policy add myminio getonly getonly.json
```



### 管理用户

```shell
# 创建用户
mc admin user add myminio newuser password

# 授权策略
mc admin policy set myminio getonly user=newuser

# 禁用用户
mc admin user disable myminio newuser

# 列出所有用户
mc admin user list myminio

```



### 管理组

```shell
# 创建组
mc admin group add myminio newgroup newuser

# 授权策略
mc admin policy set myminio getonly group=newgroup

#




# 禁用组
mc admin group disable myminio newgroup

# 列出所有组

```



## 三、使用问题

### 1、分享链接无法访问

#### `nginx代理配置`

````nginx

server {
  listen 80;
  server_name minio.dongzi.online;
  location / {
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Forwarded-Host $http_host;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_pass http://127.0.0.1:9000;
  }
}
````

#### `配置bucket访问权限`

> minio默认bucket无读取和写入权限

1、点击三个点按钮

![](./img/img_1.png)

2、编辑策略

![](./img/img_2.png)

3、添加权限

![](./img/img_3.png)





------

## 三、API

### 1、golang

>文档地址：`https://docs.minio.io/docs/golang-client-quickstart-guide.html`

`连接minio服务器`

````go
package main

import (
	"log"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func main() {
	endpoint := "minio.dongzi.online"
	accessKeyID := "dongdonghe"   // ID
	secretAccessKey := "19981014" // Secret
	useSSL := false               // ssl配置

	// Initialize minio client object.
	minioClient, err := minio.New(endpoint, &minio.Options{
		Creds:  credentials.NewStaticV4(accessKeyID, secretAccessKey, ""),
		Secure: useSSL,
	})
	if err != nil {
		log.Fatalln(err)
	}
	log.Printf("%#v\n", minioClient) // minioClient is now setup
}
````

`创建bucket`

````go
func createBucket(mc *minio.Client, bucketName string) (err error) {
	err = mc.MakeBucket(context.Background(), bucketName, minio.MakeBucketOptions{Region: "us-east-1", ObjectLocking: false}) // Region : 存储桶（us-east-1：默认）
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println("Successfully created mybucket.")
	return
}
````

`列出所有bucket`

````go
func listBucket(mc *minio.Client) (err error) {
	buckets, err := mc.ListBuckets(context.Background())
	if err != nil {
		fmt.Println(err)
		return
	}
	for _, bucket := range buckets {
		fmt.Printf("bucket名称：%s，创建时间：%s\n", bucket.Name, bucket.CreationDate)
	}
	return
}
````

`检查bucket是否存在`

````go
func bucketExits(mc *minio.Client, bucketName string) (err error) {
	found, err := mc.BucketExists(context.Background(), bucketName)
	if err != nil {
		fmt.Println(err)
		return
	}
	if found {
		fmt.Println("Bucket found")
	} else {
		fmt.Println("Bucket not found")
	}
	return
}
````

`删除bucket`

````go
func deleteBucket(mc *minio.Client, bucketName string) (err error) {
	err = mc.RemoveBucket(context.Background(), bucketName)
	if err != nil {
		fmt.Println(err)
		return
	}
	return
}
````

`获取bucket文件对象列表`

```go
func listObjs(mc *minio.Client, bucketName string) (err error) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	objectCh := mc.ListObjects(ctx, bucketName, minio.ListObjectsOptions{
		Prefix:    "制图", // 前缀条件
		Recursive: true, // 循环
	})
	for object := range objectCh {
		if object.Err != nil {
			fmt.Println(object.Err)
			return
		}
		fmt.Printf("文件名：%s，大小：%verb", object.Key, object.Size)
	}
	return
}
```

`上传文件`

````go
func uploadObject(mc *minio.Client, bucketName, objectName string, filePath string, contentType string) {
	n, err := mc.FPutObject(context.Background(),
		bucketName, // bucket
		objectName, // 上传文件名称
		filePath,   // 上传文件地址
		minio.PutObjectOptions{ContentType: contentType}) // 上传文件类型
	if err != nil {
		log.Fatalln(err)
	}

	log.Printf("Successfully uploaded %s of size %d byte \n", objectName, n.Size)
}
````

