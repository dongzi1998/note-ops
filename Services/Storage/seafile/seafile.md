# seafile

## 一、安装

### docker

```shell
docker pull seafileltd/seafile:latest

DOCKER_NAME=seafile
EXTERNAL_PORT=81
SEAFILE_ADMIN_EMAIL=admin@mail.com # 管理员账户
SEAFILE_ADMIN_PASSWORD=123123 # 管理员密码

mkdir -p  /data/$DOCKER_NAME/data/

docker run --name $DOCKER_NAME \
-d \
-e SEAFILE_ADMIN_EMAIL="$SEAFILE_ADMIN_EMAIL" \
-e SEAFILE_ADMIN_PASSWORD="$SEAFILE_ADMIN_PASSWORD" \
-v /data/$DOCKER_NAME/data/:/shared \
-p $EXTERNAL_PORT:80 \
seafileltd/seafile:latest
```

