# GItlab搭建（Centos7）

## 一、安装

### Yum

-   https://packages.gitlab.com/gitlab/gitlab-ce/

```shell
#添加gitlab-ce源
curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ce/script.rpm.sh | sudo bash
#安装
yum -y install gitlab-ce
#配置并启动gitlab-ce
gitlab-ctl reconfigure
#停止gitlab
gitlab-ctl stop
```

### Docker

```shell
#设置阿里镜像仓库
tee /etc/docker/daemon1.json <<-'EOF'
{
  "registry-mirrors": ["https://jzngeu7d.mirror.aliyuncs.com"]
}
EOF

#创建gitlab配置, 数据, 日志目录
mkdir -p /usr/local/gitlab/etc
mkdir -p /usr/local/gitlab/logs
mkdir -p /usr/local/gitlab/data

#下载gitlab镜像
docker pull beginor/gitlab-ce:11.0.1-ce.0

#运行gitlab容器
#--publish 8443:443 \    # 映射https端口, 不过本文中没有用到

docker run \
--detach \
--publish 8090:80 \      # 映射宿主机8090端口到容器中80端口
--publish 8022:22 \      # 映射22端口, 可不配
--name gitlab \            
--restart always \
--hostname 192.168.239.10 \    # 局域网宿主机的ip, 如果是公网主机可以写域名
-v /usr/local/gitlab/etc:/etc/gitlab \    # 挂载gitlab的配置文件
-v /usr/local/gitlab/logs:/var/log/gitlab \    # 挂载gitlab的日志文件
-v /usr/local/gitlab/data:/var/opt/gitlab \    # 挂载gitlab的数据
-v /etc/localtime:/etc/localtime:ro \    # 保持宿主机和容器时间同步
--privileged=true beginor/gitlab-ce    # 在容器中能以root身份执行操作
```

## 三、备份和恢复

1. gitlab版本必须一致
2. 内存需在4G以上
3. 需先部署gitlab服务再进行恢复

```shell
#修改配置文件
#vim /usr/local/gitlab/etc/gitlab.rb
vim /etc/gitlab/gitlab.rb
gitlab_rails['manage_backup_path'] = true
gitlab_rails['backup_path'] = "/data/gitlab/backups"    #gitlab备份目录
gitlab_rails['backup_archive_permissions'] = 0644       #生成的备份文件权限
gitlab_rails['backup_keep_time'] = 7776000              #备份保留天数为3个月（即90天，这里是7776000秒）
#创建备份目录åå
mkdir -p /data/gitlab/backups
chown -R git.git /data/gitlab/backups
chmod -R 777 /data/gitlab/backups
# 刷新配置
gitlab-ctl reconfigure
#备份命令
gitlab-rake gitlab:backup:create
#备份脚本使用，环境变量CRON=1的作用是如果没有任何错误发生时， 抑制备份脚本的所有进度输出
/usr/bin/gitlab-rake gitlab:backup:create CRON=1

#恢复命令
gitlab-ctl stop # 先停止，防止备份不完整
gitlab-rake gitlab:backup:restore BACKUP=1510472027_2017_11_12_9.4.5
gitlab-ctl start
#检查恢复情况
gitlab-rake gitlab:check SANITIZE=true


gitlab-ctl reconfigure

```

​	
