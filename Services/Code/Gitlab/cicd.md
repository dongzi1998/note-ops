# Gitlab-cicd

## 注册runner

```shell
# 安装gitlab-runner源
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-ci-multi-runner/script.rpm.sh | bash
# 安装gitlab-runner
yum -y install gitlab-runner
# 安装docker
yum -y install docker-ce

# 创建gitlab-ci 用户
useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash

# 将ci用户添加到dockergroup
usermod -aG docker gitlab-runner
# 注册runner
gitlab-ci-multi-runner register
>> http://192.168.239.10:8090 //gitlab_url,建议使用内网地址
>> xxxxxxxxxx 				  //注册令牌
>> my-test					  //runner说明
>> my-tag,another-tag         //tags,.gitlab-ci.yml中tags属于绑定关系
>> shell 					  //runner运行方式
```

## 编写.gitlab-ci.yml

```yaml
# docker方式部署需要填写
# image: java:8
# 该ci pipeline适合的场景
stages:
  - test
  - master
# 任务job
job:
  # 指定场景
  stags: test
  # 所需执行的脚本
  script:
    - pwd
    - ls -l 
  # 配置可用分支
  only:
  	- master
  # 指定runner ci运行
  tags:
  	- my-tag
```

