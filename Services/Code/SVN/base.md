#   Linux下的SVN服务器搭建

## 一、安装

### Yum

```shell
# 安装svnserv
yum -y install subversion
# 查看svn安装位置
rpm -ql subversion
# 创建版本库目录
mkdir /var/svn/svnrepos
# 创建svn版本库
svnadmin create /var/svn/svnrepos/xxxx # xxxx为你预期的版本库名称，可自定义
# 进入目录
cd /var/svn/svnrepos/xxxx
```


------

## 二、配置

`文件详解`

>配置文件目录：`/var/svn/svnrepos/xxx/conf`
>
>authz：负责账号权限的管理，控制账号是否读写权限
>passwd：负责账号和密码的用户名单管理
>svnserve.conf：svn服务器配置文件

`passwd`

```shell
## 等号两边有空格
账号 = 密码 
```

`authz`

```shell
# 在文件内容的末尾，添加如下：
[/]
	账号1 = rw
	账号2 = rw
# 权限目录为相对路径，绝对路径不生效
# rw表示赋予此账号可读写的权限
# 一定是反斜杠
```

`svnserve.conf`

```shell
# 取消注释
anon-access = read # 读取权限
auth-access = write # 写入权限
password-db = passwd # 指定账户文件
realm = My Firest Repository # 提示信息
```

`启动服务`

```shell
svnserve -d -r /var/svn/svnrepos
ps -aux | grep 3690
```


------

## 三、客户端登录

访问地址：`svn://ipaddress:3690/xxxx`

### 1、Windows

客户端下载地址：`https://tortoisesvn.net/downloads.html`

### 2、Mac

客户端下载地址：`https://cornerstone.assembla.com/`



------

## 四、其他

### 1、配置http登录（apache）

```shell
# 安装svn支持模块
yum install mod_dav_svn -y

# 生成svn_http用户加密文件,添加账户
htpasswd -c -m http_user houyong

# 创建svn配置文件
cd /etc/httpd/conf.d/
vim svn.conf
<Location /information>
   DAV svn
   #配置svn根目录
   SVNPath /var/svn/svnrepos/information
      AuthType Basic
      AuthName "Authorization Realm"
      # 配置http加密账户文件
      AuthUserFile /var/svn/svnrepos/information/conf/http_user
      # 配置用户权限文件
      AuthzSVNAccessFile /var/svn/svnrepos/information/conf/authz
      Satisfy all
      Require valid-user
   #</LimitExcept>
</Location>

```

