# Gogs

## 一、安装

### Binary

1、文档地址：https://gogs.io/docs/installation/install_from_binary

```shell
# 下载二进制文件
wget https://dl.gogs.io/0.12.3/gogs_0.12.3_linux_386.tar.gz

# 执行运行
./gogs web
```

