# Hugo

> 源码地址：`https://github.com/gohugoio/hugo`

## 一、安装

### brew

```shell
brew install hugo
```

### go get

```shell
go get -u -v https://github.com/gohugoio/hugo
```



## 二、使用

### 1、站点生成

```shell
# '/path/blog' 为站点路径
hugo new site /path/blog/
```

### 2、创建页面

```shell
# 生成至 站点路径/content/about.md
hugo new about.md

# 文章页一般放置在 站点路径/post/...
hugo new post/first.md
```

### 3、皮肤

> 皮肤列表：`https://www.gohugo.org/theme/`

```shell
# cd 站点目录/themes
git clone https://github.com/spf13/hyde.git  # 下载hyde皮肤
```

### 4、调试

```shell
hugo server --theme=hyde --port=8080 --buildDrafts # 默认端口1313
```

### 5、部署

```shell
# 生成build目录（通过nginx等web应用部署）
hugo --theme=hyde --baseUrl="http://blog.dongzi.online/"
```

