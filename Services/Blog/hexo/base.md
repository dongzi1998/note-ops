# hexo 使用笔记



## 一、安装

```shell
# 安装脚手架
npm install hexo-cli -g
```



## 二、建站

```shell
# 初始化
hexo init my-blog
cd my-blog
npm install
```

*目录及文件说明*

- _config.yml：网站的 [配置](https://hexo.io/zh-cn/docs/configuration) 信息
- package.json：应用程序的信息
- scaffolds：[模版](https://hexo.io/zh-cn/docs/writing) 文件夹。当您新建文章时，Hexo 会根据 scaffold 来建立文件
- source：资源文件夹是存放用户资源的地方
- themes：[主题](https://hexo.io/zh-cn/docs/themes) 文件夹

## 三、配置

