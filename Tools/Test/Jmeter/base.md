# Jmeter 笔记



## 安装

### Linux & Mac

```shell
# Download
wget https://mirrors.tuna.tsinghua.edu.cn/apache/jmeter/binaries/apache-jmeter-5.4.1.tgz

# Decompression
tar -zxvf apache-jmeter-5.4.1.tgz -C /opt/
```



## 使用

### 1、创建线程组

​				<img src="./imgs/0.png" style="zoom:25%;" />

- 设置线程数和循环次数（这里使用变量参数方式，方便通过jmeter -Pxx 传递）

- ${__P(TC,100)} : TC为参数名，100为默认值

  ​		<img src="./imgs/1.png"  style="zoom:25%;" />



### 2、配置元件

​				<img src="./imgs/2.png" style="zoom:25%">



- 设置HTTP请求默认值

- 

  <img src="./imgs/3.png" style="zoom:25%">

### 3、导出 & 命令行执行

- 导出jmx文件

<img src="./imgs/8.png" style="zoom:50%"/>

- 打开终端，执行脚本生成结果文件`demo.jtl`（Linux上操作相同）

  ​			<img src="./imgs/9.png" style="zoom:70%" />

- 生成web报告文件（Linux上操作相同）

  <img src="./imgs/10.png" />



## 脚本

### 批量压测并生成报告

```shell
#!/bin/bash			
# ./jmeter_batch.sh run 1000 192.168.0.172 31281

htmls_path=./result_htmls # web报告文件目录
scripts_path=./scripts # 脚本目录
logs_path=./logs # 压测结果目录
theard=$2 # 线程数
ip=$3 # ip
port=$4 # port
backup_path=./backup/$(date "+%Y-%m-%d_%H:%M:%S") # 备份路径

if [[ $1 = run ]]; then
    mkdir $htmls_path
    mkdir $logs_pat	h
    mkdir $htmls_path/$theard
    # 遍历脚本
    for script_path in $(find $scripts_path/* -name "*.jmx"); do
        script_name=${script_path##*/}_$theard
        # 生成结果文件
        jmeter -n -t $script_path -JTC=$theard -JIP=$ip -JPort=$port -l $logs_path/$script_name.jtl
        # 生产web报告
        jmeter -g $logs_path/$script_name.jtl -o $htmls_path/$theard/$script_name
    done
    # 打包
    # tar -czvf $htmls_path.tar.gz $htmls_path
elif
    [[ $1 = clean ]]
then
		mkdir -p $backup_path/$scripts_path
		mv $htmls_path $backup_path/
		mv $scripts_path/* $backup_path/
		mv $logs_path $backup_path/
fi

```

#### 补充脚本

```shell
#!/bin/bash
# 多线程批次执行
# ./
for ((i = $1; i < $2; i++)); do
    ./jmeter_batch.sh run $(($i * 100)) 192.168.0.172 31281
done
```



## 其他

### 限制带宽

#### wondershaper

```shell
# ----------------------------------------
# Install
yum install wondershaper
# ----------------------------------------
# Use
# Item one
# Show all network interface
ip a
# Item two
# Set upload 4Mbps download 8Mbps for eth0 interface
wondershaper eth0 4096 8192
# ----------------------------------------
# All command line options
# 参数一 网络接口名称
# 参数二 下行带宽（单位：kbps）
# 参数三 上线带宽（单位：kbps）
# -c eth0 清除适配器的限制
# -s eth0 显示适配器的当前状态
# ----------------------------------------

```

### 测速工具

#### speedtest-cli

```shell
# ----------------------------------------
# Install
pip install speedtest-cli
# ----------------------------------------
# Use
speedtest-cli
# ----------------------------------------

# –list 根据距离显示 speedtest.net 的测试服务器列表
# –server=SERVER 指定测试服务器列表中id的服务器来测试
# –share 分享测试结果，会在speedtest.net 网站上生成网速测试结果的图片
# –bytes 上传下载的带宽显示，用 bytes 代替 bits ，即 Mbyte/s 代替 Mbit/s 。换算关系： 1Mbyte/s = 8Mbit/s
```



## 