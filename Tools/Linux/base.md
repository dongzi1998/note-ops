# Linux 常用工具



## 一、网络

### 限制一个网络接口的速率

#### wondershaper

```shell
# ----------------------------------------
# Install
yum install wondershaper
# ----------------------------------------
# Use
# Item one
# Show all network interface
ip a
# Item two
# Set upload 4Mbps download 8Mbps for eth0 interface
wondershaper eth0 4096 8192
# ----------------------------------------
# All command line options
# -h 显示帮助
# -a <adapter> 设置适配器
# -d <rate> 设置最大下载速率（以Kbps为单位）和/或
# -u <rate> 设置最大上传速率（以Kbps为单位）
# -p 使用/etc/systemd/wondershaper.conf中的预设
# -c 清除适配器的限制
# -s 显示适配器的当前状态
# ----------------------------------------

```



#### trickle

```shell
# ----------------------------------------
# Install
yum -y install trickle
# ----------------------------------------
# Use
# Set upload 4Mbps download 8Mbps
trickle -u 4096 -d 8192

# ----------------------------------------
```



### 测速工具

#### speedtest-cli

```shell
# ----------------------------------------
# Install
pip install speedtest-cli
# ----------------------------------------
# Use
speedtest-cli
# ----------------------------------------

# –list 根据距离显示 speedtest.net 的测试服务器列表
# –server=SERVER 指定测试服务器列表中id的服务器来测试
# –share 分享测试结果，会在speedtest.net 网站上生成网速测试结果的图片
# –bytes 上传下载的带宽显示，用 bytes 代替 bits ，即 Mbyte/s 代替 Mbit/s 。换算关系： 1Mbyte/s = 8Mbit/s
```



## 二、监控

### 系统监控

#### htop

```shell
# ----------------------------------------
# Install
yum -y install htop
# ----------------------------------------

```

### Mysql监控

#### mtop

#### innotop





## 安全



### 检测文件或目录

#### Tripwire

```shell
# TODO：待更新
```



