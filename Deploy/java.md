# JAVA 项目部署

## 打包

```shell
maven package
```

## 部署

### jar 包

```shell
# 运行
project_name=my-huobanyun
docker_name=$($project_name)_jar
project_path=/project/$project_name

docker run --name $docker_name \
--restart=always \
--net=host \
-v /$project_path/$project_name.jar:/$PROJECT_NAME.jar \
-d primetoninc/jdk:1.8 \
java -jar \
-Duser.timezone=GMT+08 \
$PROJECT_NAME.jar \
--server.port=31283

# 扩展参数
-Duser.timezone=GMT+08  # 设置时区（东八区）
-Dserver.port=8080  # 设置端口（8080）
-XX:+UseSerialGC # 使用堆内存进行垃圾回收（默认使用专用GC线程呢）
-XX:NativeMemoryTracking=summary # 开启内存追踪
-Xss512k  # 线程堆栈内存限制（默认为1MB）
-Xms1024m  # JVM启动初始堆大小
-Xmx1536m  # JVM最大堆大小
-Xmn250m # -XX:newSize、-XX:MaxnewSize两个参数的同时配置
-XX:MaxPermSize=256M \
-XX:PermSize=128M \ # 非堆区初始内存分配大小
-XX:MaxRAM=72m \ # 非堆区分配的内存的最大上限

# 远程debug
-Xdebug # 开启调试
-Xrunjdwp:transport=dt_socket, \
server=y, \ # 表示VM 是否需要作为调试服务器执行
suspend=n,address=5005 # 远程Debug，端口5005
```

### war 包

```shell
DOCKER_NAME=project_war
PROJECT_NAME=test
# 运行容器
# docker search tomcat
docker pull docker.io/tomcat # latest
docker run --name $DOCKER_NAME \
--restart=always \
-p 8080:8080 \
-v /etc/localtime:/etc/localtime \
-d docker.io/tomcat

# 复制war包至容器内
docker cp $PROJECT_NAME.war $DOCKER_NAME:/usr/local/tomcat/webapps

# 重启容器
docker restart $DOCKER_NAME
```